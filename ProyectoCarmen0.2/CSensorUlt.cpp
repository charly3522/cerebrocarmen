/* 
* CSensorUlt.cpp
*
* Created: 24/10/2015 09:46:36 p. m.
* Author: Carlos
*/


#include "CSensorUlt.h"

// default constructor
void CSensorUlt::Inicializar(int TriggerPin, int EchoPin)
{
	m_nTrigger = TriggerPin;
	m_nEcho = EchoPin;
	pinMode(m_nTrigger, OUTPUT);
	pinMode(m_nEcho, INPUT);
	m_nuS = 0;
	m_nDistance = 0;
}

void CSensorUlt::Captar(void)
{
		delayMicroseconds(2);
		digitalWrite(m_nTrigger, HIGH);
		delayMicroseconds(10);
		digitalWrite(m_nTrigger, LOW);
		m_nuS = pulseIn(m_nEcho, HIGH, 3000);
}
unsigned int CSensorUlt::ObtenerDistancia(void)
{
	return ((m_nuS/2) / 29.1);
}
unsigned int CSensorUlt::ObtenerTiempouS(void)
{
	return m_nuS;
}
CSensorUlt::CSensorUlt()
{
} //CSensorUlt

// default destructor
CSensorUlt::~CSensorUlt()
{
} //~CSensorUlt

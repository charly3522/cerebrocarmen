/* 
* CTransmisionS.h
*
* Created: 06/11/2015 10:57:06 p. m.
* Author: Carlos
*/


#ifndef __CTRANSMISIONS_H__
#define __CTRANSMISIONS_H__

#define MAESTRO 0
#define ESCLAVO 1
#define PIN_SERVO_IZQ 2
#define PIN_SERVO_DER 3

#include <Arduino.h>
#include "CTraductor.h"
#include "MovimientoRobot.h"


class CTransmisionS
{
//variables
public:
protected:
private:
unsigned int m_unCDESU;	//Cadena de estados del sensor U
int m_nDistanciaServ;	//Distancia dada por el encoder
int m_nAnguloServ;	//AnguloDado por el encoder
int m_nVelocidadServ; //Velocidad dada por el encoder

CTraductor m_Trad;			//Traductor de sensores Ultras�nicos
int m_nTamanoCadena;    
char *m_acContCadenaEsc;//Arreglo de caracteres para el esclavo
char *m_acContCadenaMae;//Arreglo de caracteres para el maestro
MovimientoRobot m_MovRob; // Movimiento Robot
int m_OrdenDeMovimiento; //Orden de movimiento dada por cerebro 

//functions
public:
	void Inicializar();
	void ObtenerCadenaDeEstadosSenU();
	void ObtenerDistanciaDeEncoder();
	void ObtenerAnguloDeEncoder();
	void ObtenerVelocidadEncoder();
	void ConcatenarCadenas(); //S�lo para esclavo
	char RecibirTransmisiones(int DimArrRecv, int MicroReceptor);	
	void EnviarTransmisiones(int MicroEmisor);
	void Parceador(int DimArrRecv, int MicroReceptor);
	
	void ProtocoloDeEmisionEsclavo();
	void ProtocoloDeEmisionMaestro();
	
	void ProtocoloDeRecepcionEsclavo(int DimesionDeArregloARecibir);
	void ProtocoloDeRecepcionMaestro(int DimesionDeArregloARecibir);
	
	void ResetVariables();
	CTransmisionS();
	~CTransmisionS();
protected:
private:
	CTransmisionS( const CTransmisionS &c );
	CTransmisionS& operator=( const CTransmisionS &c );

}; //CTransmisionS

#endif //__CTRANSMISIONS_H__






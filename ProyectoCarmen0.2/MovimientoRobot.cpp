/* 
* MovimientoRobot.cpp
*
* Created: 27/10/2015 06:49:45 p. m.
* Author: omar
*/


#include "MovimientoRobot.h"

// default constructor
MovimientoRobot::MovimientoRobot()
{
} //MovimientoRobot

// default destructor
MovimientoRobot::~MovimientoRobot()
{
} //~MovimientoRobot
/*===========================================================================
Nombre:		Inicializar
Parametros: Nothing
Retornos:	Nothing
Descripción: Inicializa Pines para de pwm para los servos y las variables de
			 Angulo, Distanca y velocidad del robot
Autor:MEN
Fecha: 07/11/15
==============================================================================*/
void MovimientoRobot::Inicializar(char cPinServoIzquierdo,char cPinServoDerecho)
{
	Encoders_Inicializar();
	ServoDerecho.Inicializar(cPinServoDerecho);
	ServoIzquierdo.Inicializar(cPinServoIzquierdo);
	this->nAngulo = 0;
	this->nDistancia = 0;
	this->nVelocidad = 0;
}

/*===========================================================================
Nombre:		Atencion
Parametros: Recibe el tipo de movimiento a realizar 
			eParar		=0
			eAdelante	=1
			eAtras		=2
			eDerecha	=3
			eIzquierda	=4
			
Retornos:	Nothing

Descripción: Actualiza la velocidad, angulo y distancia. Mueve el robot deacuerdo al parametro recibido
			 al mandar a girar el robot con eDerecha o eIzquierda, la distancia se pone en 0 y 
			 se actualiza del valor del angulo.
Autor:MEN
Fecha: 07/11/15
==============================================================================*/
void MovimientoRobot::Atencion(uint8_t u8Movimiento)
{
static uint8_t u8MovimientoAnterior= 0;
int nDistanciaTemporal = (gsEncoder.nContVueltasDerecho * PerimetroLlanta) + (gsEncoder.nContPasosDerecho * (PerimetroLlanta/20)); 

	//si gira reinicia contadores.
	if((((u8MovimientoAnterior == eAdelante) || (u8MovimientoAnterior == eAtras)) || (u8MovimientoAnterior == eParar)) && ((u8Movimiento == eIzquierda) || (u8Movimiento == eDerecha)))
	{
		Encoder_Reset();
	}
	switch(u8Movimiento)
	{
		case eAdelante:
		this->Adelante();
		this->nDistancia = nDistanciaTemporal;
		u8MovimientoAnterior = eAdelante;
		break;
		
		case eAtras:
		this->Atras();
		this->nDistancia = nDistanciaTemporal;
		u8MovimientoAnterior = eAtras;
		break;
		
		case eDerecha:
		this->GirarDerecha();
		this->nAngulo = (nDistanciaTemporal / PerimetroGiroRobot)*360;
		if(this->nAngulo < 0)//angulos negativos
		{
			this->nAngulo = 360 + this->nAngulo;
			if(this->nAngulo <= 0) Encoder_Reset();
		}
		else//angulos positivos
		{
			if(this->nAngulo >=360) Encoder_Reset();
		}
		u8MovimientoAnterior = eDerecha;
		break;
		
		case eIzquierda:
		this->GirarIzquierda();
		this->nAngulo = (nDistanciaTemporal / PerimetroGiroRobot)*360;
		if(this->nAngulo < 0)//angulos negativos
		{
			this->nAngulo = 360 + this->nAngulo;
			if(this->nAngulo <= 0) Encoder_Reset();
		}
		else//angulos positivos
		{
			if(this->nAngulo >= 360) Encoder_Reset();
		}
		u8MovimientoAnterior = eIzquierda;
		break;
		
		case eParar:
		this->Parar();
		this->nDistancia = nDistanciaTemporal;
		u8MovimientoAnterior = eParar;
		break;
		
		default:
		break;
	}
	AtencionVelocidad(); // Atiende el valor de la velocidad
}
/*===========================================================================
Nombre:		Adelante
Parametros: Nothing
Retornos:	Nothing
Descripción: Mueve el robot hacia adelante
Autor:MEN
Fecha: 07/11/15
==============================================================================*/
void MovimientoRobot::Adelante()
{
	ServoDerecho.GiroNegativo();
	ServoIzquierdo.GiroPositivo();
}
/*===========================================================================
Nombre:		Atras
Parametros: Nothing
Retornos:	Nothing
Descripción: Mueve el robot hacia atras
Autor:MEN
Fecha: 07/11/15
==============================================================================*/
void MovimientoRobot::Atras()
{
	ServoDerecho.GiroPositivo();
	ServoIzquierdo.GiroNegativo();
}
/*===========================================================================
Nombre:		GirarDerecha
Parametros: Nothing
Retornos:	Nothing
Descripción: El robot gira a la derecha sobre su propio eje central
Autor:MEN
Fecha: 07/11/15
==============================================================================*/
void MovimientoRobot::GirarDerecha()
{
	ServoDerecho.GiroPositivo();
	ServoIzquierdo.GiroPositivo();
}
/*===========================================================================
Nombre:		GirarIzquierda
Parametros: Nothing
Retornos:	Nothing
Descripción: El robot gira a la izquierda sobre su propio eje central
Autor:MEN
Fecha: 07/11/15
==============================================================================*/
void MovimientoRobot::GirarIzquierda()
{
	ServoDerecho.GiroNegativo();
	ServoIzquierdo.GiroNegativo();
}
/*===========================================================================
Nombre:		Parar
Parametros: Nothing
Retornos:	Nothing
Descripción: Se apagan los servos.(apaga pwm) 
			 Cuidado: al apagar los servos el robot se puede mover por gravedad.
Autor:MEN
Fecha: 07/11/15
==============================================================================*/
void MovimientoRobot::Parar()
{
	ServoDerecho.Detener();
	ServoIzquierdo.Detener();
}
/*===========================================================================
Nombre:		AtencionVelocidad
Parametros: Nothing
Retornos:	Nothing
Descripción: Actualiza la velocidad cada 1 seg.
Autor:MEN
Fecha: 07/11/15
==============================================================================*/

void MovimientoRobot::AtencionVelocidad()
{
static int D0 = 0;
static int D1 = 0;

	D1 = this->nDistancia;
	if(Timer1.nListaTimers[Timer1.eTimerMotors] == 0)
	{
		Timer1.nListaTimers[Timer1.eTimerMotors] = 1000;
		this->nVelocidad = D1 - D0;
		D0 = D1;
	}
}
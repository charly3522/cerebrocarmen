/* 
* CTransmisionS.cpp
*
* Created: 06/11/2015 10:57:05 p. m.
* Author: Carlos
*/


#include "CTransmisionS.h"

// default constructor
void CTransmisionS::Inicializar()
{
	m_unCDESU = 0;
	m_nDistanciaServ = 0;
	m_nAnguloServ = 0;
	m_nVelocidadServ = 0;
	m_Trad.Inicializar(10);
	Serial2.begin(9600);
	m_nTamanoCadena = 0;
	m_acContCadenaEsc = (char*)malloc(m_nTamanoCadena * sizeof(char));
	m_acContCadenaMae = (char*)malloc(m_nTamanoCadena * sizeof(char));
	m_MovRob.Inicializar(PIN_SERVO_IZQ, PIN_SERVO_DER);
	m_OrdenDeMovimiento = -1;
}
void CTransmisionS::ObtenerCadenaDeEstadosSenU()
{
	m_unCDESU = m_Trad.ObtenerCadenaDeEstados();
}
void CTransmisionS::ObtenerDistanciaDeEncoder()
{
	m_nDistanciaServ = m_MovRob.nDistancia;
}
void CTransmisionS::ObtenerAnguloDeEncoder()
{
	m_nAnguloServ = m_MovRob.nAngulo;
}
void CTransmisionS::ObtenerVelocidadEncoder()
{
	m_nVelocidadServ = m_MovRob.nVelocidad;
}
void CTransmisionS::ConcatenarCadenas()
{
	String sCadena = "";
	// Se concatena los datos en un string
	sCadena += '+';
	sCadena += m_unCDESU;
	sCadena += ',';
	sCadena += m_nDistanciaServ;
	sCadena += ',';
	sCadena += m_nAnguloServ;
	sCadena += ',';
	sCadena += m_nVelocidadServ;
	
	// Se redimensiona la cadena de caracteres para guardarse
	m_nTamanoCadena = sCadena.length();
	m_acContCadenaEsc = (char*)malloc(m_nTamanoCadena* sizeof(char));
	sCadena.toCharArray(m_acContCadenaEsc, m_nTamanoCadena);
	// El arreglo concatenado es guardado en m_acContCadena

}
char CTransmisionS::RecibirTransmisiones(int DimArrRecv, int MicroReceptor)
{
	char mas[1];
	Serial2.readBytes(mas,1);	
	if(mas[0] ==  '+')
	{
		if(MicroReceptor == MAESTRO)
		{
			m_acContCadenaMae = (char*)malloc((DimArrRecv - 1) * sizeof(char));
			Serial2.readBytes(m_acContCadenaMae, DimArrRecv - 1);	
		}
		else if(MicroReceptor == ESCLAVO)
		{
			m_acContCadenaEsc = (char*)malloc((DimArrRecv - 1) * sizeof(char));
			Serial2.readBytes(m_acContCadenaEsc, DimArrRecv - 1);	
		}
		return 1;
			
	}
	return 0;
}
void CTransmisionS::EnviarTransmisiones(int MicroEmisor)
{
	if(MicroEmisor == MAESTRO)
	{
		Serial2.write(m_acContCadenaMae);	
	}
	else if (MicroEmisor == ESCLAVO)
	{
		Serial2.write(m_acContCadenaEsc);
	}
	
}
void CTransmisionS::Parceador(int DimArrRecv, int MicroReceptor)
{
	String Cadena = "";
	char cState = 0;
	if(MicroReceptor == ESCLAVO)
	{
		for(int i = 0; i < DimArrRecv; i++)
		{
			Cadena += m_acContCadenaEsc[i];	
		}
		m_OrdenDeMovimiento = Cadena.toInt();
	}
	else if (MicroReceptor == MAESTRO)
	{
		// Existen cuatro variables a escribir
		// Primera es m_unCDESU
		// Segunda es m_nDistanciaServ
		// Tercera es m_nAnguloServ
		// Cuarta  es m_nVelocidadServ
		for(int i = 0; i < DimArrRecv; i++)
		{
			if(m_acContCadenaMae[i] == ',') // Termin� de leer una de las variables
			{
				cState ++;
				if(cState == 1){
					m_unCDESU = Cadena.toInt();
				}else if(cState == 2){
					m_nDistanciaServ = Cadena.toInt();
				}else if(cState == 3){
					m_nAnguloServ = Cadena.toInt();
				}else if(cState == 4){
					m_nVelocidadServ = Cadena.toInt();
				}
				i++;
				Cadena = "";				
			}
			Cadena += m_acContCadenaMae[i];
		}
	}
	
}
void CTransmisionS::ProtocoloDeEmisionEsclavo()
{
	///Algoritmo para transmisor micro esclavo///
	/*
	1. Solicitar datos
	1.1 cadena de estados de sensoresU
	1.2 Distancia recorrida de servo
	1.3 Angulo actual del vehiculo
	2. Concatenar datos para protocolo de envio
	3. Enviar */
	ObtenerCadenaDeEstadosSenU();
	ObtenerDistanciaDeEncoder();
	ObtenerAnguloDeEncoder();
	ConcatenarCadenas();
	EnviarTransmisiones(ESCLAVO);
	
		
}
void CTransmisionS::ProtocoloDeEmisionMaestro()//Necesario Crear Cerebro
{
	/*
	/// Algoritmo para transmisor micro maestro///
	1. CCerebro le otorga datos
	2. CCerebro ordena concatenarlos para protoco de envio
	3. CCerebro solicita enviar datos*/
	
}
void CTransmisionS::ProtocoloDeRecepcionEsclavo(int DimesionDeArregloARecibir)
{
	/*
	/// Algoritmo para emisor micro esclavo ///
	1. Recibe datos para servomotores
	2. Parcea
	3. envia ordenes de movimiento a objeto CmovimientoRobot*/
	RecibirTransmisiones(DimesionDeArregloARecibir, ESCLAVO);
	Parceador((DimesionDeArregloARecibir - 1),ESCLAVO);
	//Enviar Orden a MovimientoRobot
}
void CTransmisionS::ProtocoloDeRecepcionMaestro(int DimesionDeArregloARecibir)
{
	/*/// Algoritmo de emisor micro maestro ///
	1. Recibe datos
	2. Parcea
	3. Envia datos a Ccerebro*/
	RecibirTransmisiones(DimesionDeArregloARecibir, MAESTRO);
	Parceador((DimesionDeArregloARecibir - 1), MAESTRO);
	//Enviar Datos a Cerebro
}
void CTransmisionS::ResetVariables()
{
	m_acContCadenaMae = (char *)malloc(sizeof(char));
	m_acContCadenaMae[0] = '0';
	m_acContCadenaEsc = (char *)malloc(sizeof(char));
	m_acContCadenaEsc[0] = '0';
	
}
CTransmisionS::CTransmisionS()
{
} //CTransmisionS

// default destructor
CTransmisionS::~CTransmisionS()
{
} //~CTransmisionS

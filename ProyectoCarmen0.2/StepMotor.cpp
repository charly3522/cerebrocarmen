 /* 
* StepMotor.cpp
*
* Created: 12/09/2015 05:11:08 p. m.
* Author: omar
*/


#include "StepMotor.h"
#include "Arduino.h"
#include "TimerOne.h"

// default constructor
StepMotor::StepMotor()
{
} //StepMotor

// default destructor
StepMotor::~StepMotor()
{
} //~StepMotor

/*=============================================
Nombre:		Inicializar
Parametros: Nothing
Retornos:	Nothing
Descripción: Inicializa step motor
Autor:		
===============================================*/
void StepMotor::Inicializar()
{
	pinMode(STEP_COIL1_A, OUTPUT);
	pinMode(STEP_COIL1_B, OUTPUT);
	pinMode(STEP_COIL1_C, OUTPUT);
	pinMode(STEP_COIL1_D, OUTPUT);
	digitalWrite(STEP_COIL1_A, LOW);
	digitalWrite(STEP_COIL1_B, LOW);
	digitalWrite(STEP_COIL1_C, LOW);
	digitalWrite(STEP_COIL1_D, LOW);
	pinMode(STEP_COIL2_A, OUTPUT);
	pinMode(STEP_COIL2_B, OUTPUT);
	pinMode(STEP_COIL2_C, OUTPUT);
	pinMode(STEP_COIL2_D, OUTPUT);
	digitalWrite(STEP_COIL2_A, LOW);
	digitalWrite(STEP_COIL2_B, LOW);
	digitalWrite(STEP_COIL2_C, LOW);
	digitalWrite(STEP_COIL2_D, LOW);
	Timer1.nListaTimers[Timer1.eTimerMotors]=0; //timer
}

/*=============================================
Nombre:Forward
Parametros: nothing
Retornos:	nothing
Descripción: Hace avanzar 2 motores a pasos.
Autor: Men 12/09/15
===============================================*/
void StepMotor::Forward()
{
	static char State = ePaso1;
	static char cToggle = 0;
	if (Timer1.nListaTimers[Timer1.eTimerMotors] == 0) 
		Timer1.nListaTimers[Timer1.eTimerMotors] = Tiempo_Step;
	else return;	
	switch (State)
	{
		case ePaso1:
			StepMotor1_PolarizadoA
			StepMotor2_PolarizadoA
			cToggle ^= 1;
			State = ePaso2;
			break;
		
		case ePaso2:
			StepMotor1_PolarizadoB
			StepMotor2_PolarizadoB
			State = ePaso3;
			cToggle ^= 1;
			break;
		case ePaso3:
			StepMotor1_PolarizadoC
			StepMotor2_PolarizadoC 
			State = ePaso4;
			cToggle ^= 1;
			break;
		case ePaso4:
			StepMotor1_PolarizadoD
			StepMotor2_PolarizadoD
			State = ePaso1;
			cToggle ^= 1;
			break;
		
		default:
			State = ePaso1;
			break;
	}
	digitalWrite(53,cToggle);
}

/*=============================================
Nombre:Reverse
Parametros: nothing
Retornos:	nothing
Descripción: Hace retroceder 2 motores a pasos.
Autor: Men 12/09/15
===============================================*/
void StepMotor::Reverse()
{
	static char State = ePaso4;
	if (Timer1.nListaTimers[Timer1.eTimerMotors] == 0)
	Timer1.nListaTimers[Timer1.eTimerMotors] = Tiempo_Step;
	else return;
	switch (State)
	{
		case ePaso1:
			StepMotor1_PolarizadoD
			StepMotor2_PolarizadoD
			State = ePaso4;
			break;
		case ePaso2:
			StepMotor1_PolarizadoC
			StepMotor2_PolarizadoC
			State = ePaso1;
			break;
		case ePaso3:
			StepMotor1_PolarizadoB
			StepMotor2_PolarizadoB
			State = ePaso2;
			break;
		case ePaso4:
			StepMotor1_PolarizadoA
			StepMotor2_PolarizadoA
			State = ePaso3;
			break;
		
		default:
			State = ePaso4;
			break;
	}
}

/*=============================================
Nombre:ReverseStepMotor
Parametros: nothing
Retornos:	nothing
Descripción: Hace retroceder 2 motores a pasos.
Autor: Men 12/09/15
===============================================*/
void StepMotor::RotateCenterAxisRight()
{
	static char State = ePaso1;
	if (Timer1.nListaTimers[Timer1.eTimerMotors] == 0)
	Timer1.nListaTimers[Timer1.eTimerMotors] = Tiempo_Step;
	else return;
	switch (State)
	{
		case ePaso1:
			StepMotor1_PolarizadoA
			StepMotor2_PolarizadoD
			State = ePaso2;
			break;
		case ePaso2:	
			StepMotor1_PolarizadoB
			StepMotor2_PolarizadoC
			State = ePaso3;
			break;
		case ePaso3:
			StepMotor1_PolarizadoC
			StepMotor2_PolarizadoB
			State = ePaso4;
			break;
		case ePaso4:
			StepMotor1_PolarizadoD
			StepMotor2_PolarizadoA
			State = ePaso1;
			break;
		
		default:
		State = ePaso1;
		break;
	}
	
}

void StepMotor::RotateCenterAxisLeft()
{
	static char State = ePaso1;
	
	if (Timer1.nListaTimers[Timer1.eTimerMotors] == 0)
	Timer1.nListaTimers[Timer1.eTimerMotors] = Tiempo_Step;
	else return;
	
	switch (State)
	{
		case ePaso1:
			StepMotor1_PolarizadoD
			StepMotor2_PolarizadoA
			State = ePaso2;
			break;
		
		case ePaso2:
			StepMotor1_PolarizadoC
			StepMotor2_PolarizadoB
			State = ePaso3;
			break;
		
		case ePaso3:
			StepMotor1_PolarizadoB
			StepMotor2_PolarizadoC
			State = ePaso4;
			break;
		
		case ePaso4:
			StepMotor1_PolarizadoA
			StepMotor2_PolarizadoD
			State = ePaso1;
			break;
		
		default:
			State = ePaso1;
			break;
	}
}


void StepMotor::RotateRightAxisForward (void)
{
	static char State = ePaso1;
	
	if (Timer1.nListaTimers[Timer1.eTimerMotors] == 0)
		Timer1.nListaTimers[Timer1.eTimerMotors] = Tiempo_Step;
	else return;
	
	switch (State)
	{
		case ePaso1:
			StepMotor1_PolarizadoA
			StepMotor2_PolarizadoA
			State = ePaso2;
			break;
		case ePaso2:
			StepMotor1_PolarizadoA
			StepMotor2_PolarizadoB
			State = ePaso3;
			break;
		
		case ePaso3:
			StepMotor1_PolarizadoA
			StepMotor2_PolarizadoC
			State = ePaso4;
			break;
		
		case ePaso4:
			StepMotor1_PolarizadoA
			StepMotor2_PolarizadoD
			State = ePaso1;
			break;
		
		default:
			State = ePaso1;
			break;
	}
}

void StepMotor::RotateRightAxisReverse (void)
{
	static char State = ePaso1;
	
	if (Timer1.nListaTimers[Timer1.eTimerMotors] == 0)
	Timer1.nListaTimers[Timer1.eTimerMotors] = Tiempo_Step;
	else return;
	
	switch (State)
	{
		case ePaso1:
			StepMotor1_PolarizadoA
			StepMotor2_PolarizadoD
			State = ePaso2;
			break;
		
		case ePaso2:
			StepMotor1_PolarizadoA
			StepMotor2_PolarizadoC
			State = ePaso3;
			break;
		
		case ePaso3:
			StepMotor1_PolarizadoA
			StepMotor2_PolarizadoB
			State = ePaso4;
			break;
		
		case ePaso4:
			StepMotor1_PolarizadoA
			StepMotor2_PolarizadoA
			State = ePaso1;
			break;
		
		default:
			State = ePaso1;
			break;
	}
}

void StepMotor::RotateLeftAxisForward	(void)
{
	static char State = ePaso1;
	
	if (Timer1.nListaTimers[Timer1.eTimerMotors] == 0)
	Timer1.nListaTimers[Timer1.eTimerMotors] = Tiempo_Step;
	else return;
	
	switch (State)
	{
		case ePaso1:
			StepMotor1_PolarizadoA
			StepMotor2_PolarizadoA
			State = ePaso2;
			break;
		
		case ePaso2:
			StepMotor1_PolarizadoB
			StepMotor2_PolarizadoA
			State = ePaso3;
			break;
		
		case ePaso3:
			StepMotor1_PolarizadoC
			StepMotor2_PolarizadoA
			State = ePaso4;
			break;
	
		case ePaso4:
			StepMotor1_PolarizadoD
			StepMotor2_PolarizadoA
			State = ePaso1;
			break;
		
		default:
			State = ePaso1;
			break;
	}
}

void StepMotor::RotateLeftAxisReverse	(void)
{
	static char State = ePaso1;
	
	if (Timer1.nListaTimers[Timer1.eTimerMotors] == 0)
	Timer1.nListaTimers[Timer1.eTimerMotors] = Tiempo_Step;
	else return;
	
	switch (State)
	{
		case ePaso1:
			StepMotor1_PolarizadoD
			StepMotor2_PolarizadoA
			State = ePaso2;
			break;
		
		case ePaso2:
			StepMotor1_PolarizadoC
			StepMotor2_PolarizadoA
			State = ePaso3;
			break;
		
		case ePaso3:
			StepMotor1_PolarizadoB
			StepMotor2_PolarizadoA
			State = ePaso4;
			break;
		
		case ePaso4:
			StepMotor1_PolarizadoA
			StepMotor2_PolarizadoA
			State = ePaso1;
			break;
		
		default:
			State = ePaso1;
			break;
	}
}

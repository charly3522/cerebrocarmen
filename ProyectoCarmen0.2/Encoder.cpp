// 
// 
// 
#include "Encoder.h"
sEncoder gsEncoder;

/*================================================================================
Nombre:		Encoder_Inicializaralizar
Parametros: Nothing
Retornos:	Nothing
Descripción: Habilita interrupciones para detectar el cambio de estado de 
			 HIGH A LOW del pin CLK del encoder, y inicializa pines del micro
Autor:Men
Fecha: 01/11/15
==================================================================================*/
void Encoders_Inicializar()
{
	pinMode(Encoder_PinCLKDerecho,INPUT);
	pinMode(Encoder_PinCLKIzquierdo,INPUT);
	pinMode(Encoder_PinDTDerecho,INPUT);
	pinMode(Encoder_PinDTIzquierdo,INPUT);
	attachInterrupt(digitalPinToInterrupt(Encoder_PinCLKDerecho),Encoder_IsrEncoderDerecho,FALLING);
	attachInterrupt(digitalPinToInterrupt(Encoder_PinCLKIzquierdo),Encoder_IsrEncoderIzquierdo,FALLING);
	gsEncoder.nContPasosIzquierdo = 0;
	gsEncoder.nContPasosDerecho =	0;
	gsEncoder.nContVueltasDerecho=	0;
	gsEncoder.nContVueltasIzquierdo=0 ; 
}


/*=============================================
Nombre:		Encoder_IsrEncoderDerecho
Parametros: Nothing
Retornos:	Nothing
Descripción: Funcion a se llamada por la interrucion generada
			 al cambiar el pin CLK HIGH A LOW
Autor:MEN
Fecha: 01/11/15
===============================================*/
void Encoder_IsrEncoderDerecho()
{
	static long lTiempoAnt = 0;
	long lTiempo = millis();
	if(lTiempo - lTiempoAnt > Encoder_TiempoAntiRebote)//antirebote.
	{
		//contador pasos
		if(digitalRead(Encoder_PinDTDerecho))
		{
			 gsEncoder.nContPasosDerecho++;
			 gsEncoder.eSentidoGiroDerecho = eEncoder_GiroPositivo;
		}
		else								  
		{
			gsEncoder.nContPasosDerecho--;
			gsEncoder.eSentidoGiroDerecho = eEncoder_GiroNegativo;
		}
		//contador vueltas
		if(gsEncoder.nContPasosDerecho == Encoder_MaxNumeroPasos)
		{
			gsEncoder.nContPasosDerecho = 0;
			gsEncoder.nContVueltasDerecho ++;
		}
		if(gsEncoder.nContPasosDerecho == -Encoder_MaxNumeroPasos)
		{
			gsEncoder.nContPasosDerecho = 0;
			gsEncoder.nContVueltasDerecho --;
		}
	}
	lTiempoAnt = lTiempo;
}

/*=============================================
Nombre:		Encoder_IsrEncoderIzquierdo
Parametros: Nothing
Retornos:	Nothing
Descripción: Funcion a se llamada por la interrucion generada
			 al cambiar el pin CLK HIGH A LOW
Autor:MEN
Fecha: 01/11/15
===============================================*/
void Encoder_IsrEncoderIzquierdo()
{
	static long lTiempoAnt = 0;
	long lTiempo = millis();
	if(lTiempo - lTiempoAnt > Encoder_TiempoAntiRebote)//antirebote.
	{
		if(digitalRead(Encoder_PinDTIzquierdo)) 
		{
			gsEncoder.nContPasosIzquierdo++;
			gsEncoder.eSentidoGiroIzquierdo = eEncoder_GiroPositivo;
		}
		else								
		{	
			gsEncoder.nContPasosIzquierdo--;
			gsEncoder.eSentidoGiroIzquierdo = eEncoder_GiroNegativo;
		}
		
		if(gsEncoder.nContPasosIzquierdo == Encoder_MaxNumeroPasos)
			{
				gsEncoder.nContPasosIzquierdo = 0;
				gsEncoder.nContVueltasIzquierdo ++;
			}
		if(gsEncoder.nContPasosIzquierdo == -Encoder_MaxNumeroPasos)
		{
			gsEncoder.nContPasosIzquierdo = 0;
			gsEncoder.nContVueltasIzquierdo --;
		}
	}
	lTiempoAnt = lTiempo;
}

/*=============================================
Nombre:		Encoder_Reset
Parametros: Nothing
Retornos:	Nothing
Descripción: Reinicia contadores
Autor:MEN
Fecha: 01/11/15
===============================================*/
void Encoder_Reset()
{
		gsEncoder.nContPasosIzquierdo = 0;
		gsEncoder.nContPasosDerecho =	0;
		gsEncoder.nContVueltasDerecho=	0;
		gsEncoder.nContVueltasIzquierdo=0 ;
}
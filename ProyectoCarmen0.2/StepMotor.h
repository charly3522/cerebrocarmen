/* 
* StepMotor.h
*
* Created: 12/09/2015 05:11:09 p. m.
* Author: omar
*/


#ifndef __STEPMOTOR_H__
#define __STEPMOTOR_H__


class StepMotor
{
public:
/*====================================
	Definiciones de hardware
======================================*/
//Motor 1
#define Tiempo_Step	  20//ms
#define STEP_COIL1_A  47
#define STEP_COIL1_B  49
#define STEP_COIL1_C  51
#define STEP_COIL1_D  53
//Motor 2
#define STEP_COIL2_A  41
#define STEP_COIL2_B  39
#define STEP_COIL2_C  37
#define STEP_COIL2_D  35
 

/*====================================
	Enums y Diagramas de estados
======================================*/
private:
typedef enum
{
	ePaso1=0,
	ePaso2,
	ePaso3,
	ePaso4
}EdoStep;

bool m_bStop;

#define StepMotor1_PolarizadoA	digitalWrite(STEP_COIL1_A, HIGH);digitalWrite(STEP_COIL1_B, HIGH);digitalWrite(STEP_COIL1_C, LOW);digitalWrite(STEP_COIL1_D, LOW);
#define StepMotor1_PolarizadoB	digitalWrite(STEP_COIL1_A, LOW);digitalWrite(STEP_COIL1_B, HIGH);digitalWrite(STEP_COIL1_C, HIGH);digitalWrite(STEP_COIL1_D, LOW);
#define StepMotor1_PolarizadoC	digitalWrite(STEP_COIL1_A, LOW); digitalWrite(STEP_COIL1_B, LOW); digitalWrite(STEP_COIL1_C, HIGH); digitalWrite(STEP_COIL1_D, HIGH);
#define StepMotor1_PolarizadoD  digitalWrite(STEP_COIL1_A, HIGH); digitalWrite(STEP_COIL1_B, LOW); digitalWrite(STEP_COIL1_C, LOW); digitalWrite(STEP_COIL1_D, HIGH);

#define StepMotor2_PolarizadoA	digitalWrite(STEP_COIL2_A, HIGH);digitalWrite(STEP_COIL2_B, HIGH);digitalWrite(STEP_COIL2_C, LOW);digitalWrite(STEP_COIL2_D, LOW);
#define StepMotor2_PolarizadoB	digitalWrite(STEP_COIL2_A, LOW);digitalWrite(STEP_COIL2_B, HIGH);digitalWrite(STEP_COIL2_C, HIGH);digitalWrite(STEP_COIL2_D, LOW);
#define StepMotor2_PolarizadoC 	digitalWrite(STEP_COIL2_A, LOW);digitalWrite(STEP_COIL2_B, LOW);digitalWrite(STEP_COIL2_C, HIGH);digitalWrite(STEP_COIL2_D, HIGH);
#define StepMotor2_PolarizadoD	digitalWrite(STEP_COIL2_A, HIGH);digitalWrite(STEP_COIL2_B, LOW);digitalWrite(STEP_COIL2_C, LOW);digitalWrite(STEP_COIL2_D, HIGH);

protected:
//functions
public:
	void Inicializar			(void);
    void Forward				(void);
	void Reverse				(void);
	void RotateCenterAxisRight	(void);	
	void RotateCenterAxisLeft	(void);
	void RotateRightAxisForward (void);
	void RotateRightAxisReverse (void);
	void RotateLeftAxisForward	(void);
	void RotateLeftAxisReverse	(void);
	StepMotor();
	~StepMotor();
protected:
private:
	StepMotor( const StepMotor &c );
	StepMotor& operator=( const StepMotor &c );

}; //StepMotor

#endif //__STEPMOTOR_H__

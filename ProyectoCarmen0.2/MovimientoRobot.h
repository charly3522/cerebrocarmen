/* 
* MovimientoRobot.h
*
* Created: 27/10/2015 06:49:46 p. m.
* Author: omar
*/


#ifndef __MOVIMIENTOROBOT_H__
#define __MOVIMIENTOROBOT_H__
#include "Servomotor.h"
#include "Encoder.h"
#include "TimerOne.h"


#define	eParar		0
#define	eAdelante	1
#define	eAtras		2
#define eDerecha	3
#define eIzquierda	4


class MovimientoRobot
{
//variables
public:
#define PerimetroLlanta		31.41 //cm  
#define PerimetroGiroRobot	84.38 //cm

int nAngulo;  //posicion como angulo del robot cuando gira de: 0� <-> 360�
int nDistancia; //distancia que a avanzado el robot desde marco de referencia. (-32767cm) <-> (32766cm) 
int nVelocidad; // velocidad del robot en cm/s
protected:

private:

//functions
public:
	void Inicializar(char cPinServoIzquierdo,char cPinServoDerecho);
	void Atencion(uint8_t u8Movimiento);
	MovimientoRobot();
	~MovimientoRobot();
protected:
private:
	MovimientoRobot( const MovimientoRobot &c );
	MovimientoRobot& operator=( const MovimientoRobot &c );
	void Adelante();
	void Atras();
	void GirarDerecha();
	void GirarIzquierda();
	void Parar();
	void AtencionVelocidad();
	Servomotor	ServoIzquierdo;
	Servomotor	ServoDerecho;

}; //MovimientoRobot

#endif //__MOVIMIENTOROBOT_H__

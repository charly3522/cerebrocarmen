/* 
* CCerebro.h
*
* Created: 08/11/2015 01:48:15 a. m.
* Author: Carlos
*/


#ifndef __CCEREBRO_H__
#define __CCEREBRO_H__


class CCerebro
{
//variables
public:
protected:
private:

//functions
public:
	CCerebro();
	~CCerebro();
protected:
private:
	CCerebro( const CCerebro &c );
	CCerebro& operator=( const CCerebro &c );

}; //CCerebro

#endif //__CCEREBRO_H__

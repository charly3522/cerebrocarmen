/* 
* CSensorUlt.h
*
* Created: 24/10/2015 09:46:36 p. m.
* Author: Carlos
*/


#include <Arduino.h>
#include "TimerOne.h"

#ifndef __CSENSORULT_H__
#define __CSENSORULT_H__


class CSensorUlt
{
//variables
public:
protected:
private:
//Trigger y Ecco
unsigned m_nTrigger, m_nEcho;
//Medidor de microsegundos
unsigned int m_nuS;
//Distancia
int m_nDistance;

//functions
public:
	void Inicializar(int TriggerPin, int EchoPin);
	void Captar(void);
	//int MedianaDistancia(void);
	unsigned int ObtenerDistancia(void);
	//int ObtenerDistanciaFinal(void);
	unsigned int ObtenerTiempouS(void);
	CSensorUlt();
	~CSensorUlt();
protected:
private:
	CSensorUlt( const CSensorUlt &c );
	CSensorUlt& operator=( const CSensorUlt &c );

}; //CSensorUlt

#endif //__CSENSORULT_H__

/* 
* Servomotor.cpp
*
* Created: 10/10/2015 05:31:56 p. m.
* Author: omar
*/


#include "Servomotor.h"
// default constructor
Servomotor::Servomotor()
{
} //Servomotor

// default destructor
Servomotor::~Servomotor()
{
	
} //~Servomotor

void Servomotor::Inicializar(int pin)
{
	this->MyServo.attach(pin);
	this-> nPin = pin;
}


void Servomotor::GiroPositivo()
{
	if(!this->MyServo.attached())this->MyServo.attach(nPin); //si esta detenido el servo, lo habilita
		this->MyServo.write(ValorGiroPositivo);
} 

void Servomotor::GiroNegativo()
{
	if(!this->MyServo.attached())this->MyServo.attach(nPin); //si esta detenido el servo, lo habilita
		this->MyServo.write(ValorGiroNegativo);
}

void Servomotor::Detener()
{
	this->MyServo.detach();
}
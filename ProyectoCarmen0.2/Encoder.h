// Encoder2.h

#ifndef _ENCODER2_h
#define _ENCODER2_h
///////////selecciona encabezado de arduino//////////////////
#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
/////////////////////////////////////////////////////////////
/*
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

Libreria enconder
Funcion: Controlar 2 encoder, guadando sus pasos en la estructura gsEncoder

Nota: esta librer�a fue escrita en unicamente en lenguaje C, sin hacer uso de clases,
	 para poder hacer uso de interrupciones de hardware.
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 
*/
/*====================================
	Definiciones de hardware
======================================*/
#define Encoder_PinDTIzquierdo	6
#define	Encoder_PinDTDerecho	5
#define Encoder_PinCLKIzquierdo 19 //solo usa pines(arduino mega): 2,3,18,19,20,21 
#define Encoder_PinCLKDerecho	18//solo usa pines(arduino mega): 2,3,18,19,20,21 

#define Encoder_TiempoAntiRebote 10 //valor en ms
#define Encoder_MaxNumeroPasos   20 //Pasos que tiene el encoder


/*====================================
	Enums y Diagramas de estados
======================================*/
typedef enum
{
	eEncoder_GiroPositivo = 0,
	eEncoder_GiroNegativo,
	
}eEncoder_SentidoGiro;


/*====================================
	Estructuras de datos
======================================*/

typedef struct  
{
	int8_t					nContPasosIzquierdo;
	int8_t					nContPasosDerecho;
	int						nContVueltasDerecho;
	int						nContVueltasIzquierdo;
	eEncoder_SentidoGiro	eSentidoGiroDerecho;
	eEncoder_SentidoGiro	eSentidoGiroIzquierdo;
}sEncoder;

/*====================================
	Varibles Externas 
======================================*/

extern sEncoder gsEncoder;

/*====================================
	Funciones
======================================*/
void	Encoders_Inicializar			(void);
void	Encoder_IsrEncoderDerecho		(void);
void	Encoder_IsrEncoderIzquierdo		(void);
void	Encoder_Reset					(void);


#endif


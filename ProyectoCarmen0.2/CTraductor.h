/* 
* CTraductor.h
*
* Created: 27/10/2015 06:39:37 p. m.
* Author: Carlos
*/
#ifndef __CTRADUCTOR_H__
#define __CTRADUCTOR_H__

// PINES PARA SENSORES U ///
/* Pares son triggers
PIN 22
PIN 24
PIN 26
PIN 28
PIN 30
PIN 32
PIN 34
PIN 36
PIN 38
PIN 40
/* Impares son Eccho
PIN 23
PIN 25
PIN 27
PIN 29
PIN 31
PIN 33
PIN 35
PIN 37
PIN 39
PIN 41
*/
typedef enum {
	ePinSensorUTrigger01 = 22,
	ePinSensorUEccho01,
	ePinSensorUTrigger02,
	ePinSensorUEccho02,
	ePinSensorUTrigger03,
	ePinSensorUEccho03,
	ePinSensorUTrigger04,
	ePinSensorUEccho04,
	ePinSensorUTrigger05,
	ePinSensorUEccho05,
	ePinSensorUTrigger06,
	ePinSensorUEccho06,
	ePinSensorUTrigger07,
	ePinSensorUEccho07,
	ePinSensorUTrigger08,
	ePinSensorUEccho08,
	ePinSensorUTrigger09,
	ePinSensorUEccho09,
	ePinSensorUTrigger10,
	ePinSensorUEccho10
	 
	}ePinSensors;
	/////////Nomenclatura de sensores////////
	/*
	Acrónimo	Pines	Descripción
	
	SU_SDI		22-23	Sensor ultrasónico superior delantero izquierdo
	SU_SDD		24-25	Sensor ultrasónico superior delantero derecho
	SU_SLI		26-27	Sensor ultrasónico superior lateral izquierdo
	SU_SLD		28-29	Sensor ultrasónico superior lateral derecho
	SU_STI		30-31	Sensor ultrasónico superior trasero izquierdo
	SU_STD		32-33	Sensor ultrasónico superior trasero derecho
	SU_IDI		34-35	Sensor ultrasónico inferior delantero izquierdo
	SU_IDD		36-37	Sensor ultrasónico inferior delantero derecho
	SU_ITI		38-39	Sensor ultrasónico inferior trasero izquierdo	
	SU_ITD		40-41	Sensor ultrasónico inferior trasero derecho
	*/
	typedef enum{
		SU_SDI = 0,
		SU_SDD,
		SU_SLI,
		SU_SLD,
		SU_STI,
		SU_STD,
		SU_IDI,
		SU_IDD,
		SU_ITI,
		SU_ITD
	}eSensor;
	////Limites para cada sensor en cm ////
	#define LIMITE_SU_SDI 10
	#define LIMITE_SU_SDD 10
	#define LIMITE_SU_SLI 10
	#define LIMITE_SU_SLD 10
	#define LIMITE_SU_STI 10
	#define LIMITE_SU_STD 10
	#define LIMITE_SU_IDI 10
	#define LIMITE_SU_IDD 10
	#define LIMITE_SU_ITI 10
	#define LIMITE_SU_ITD 10
		
#include <Arduino.h>
#include "CSensorUlt.h"
#include "TimerOne.h"



class CTraductor
{
//variables
public:
protected:
private:
int m_ConjuntoMuestrasSensorU[10][10]; //Filas: Sensores; columnas: Muestras por sensor; //Sin usar por ahora
int m_snContadorMuestras[10]; //sin usar por ahora
String CB; //string para la conversion de decimal binario //sin usar por ahora

CSensorUlt m_CSU[10]; //conjunto de objetos del tipo sensor ultrasónico
int m_nConjuntoDatos[10]; //Conjunto de distancias de los diez sensores
int m_CantidadDeSensores; //Cantidad preestablecida de sensores
unsigned int m_nCBDE; //Cadena en la que se encierran los estados de todos los sensores



//functions
public:
	void Inicializar(int NumberOfSensors);
	void ObtenerDatosDeSensores();
	void MostrarDatosSenUSerial();
	void CrearCadenaDeEstados();
	unsigned int ObtenerCadenaDeEstados();
	
	
	CTraductor();
	~CTraductor();
protected:
private:
	CTraductor( const CTraductor &c );
	CTraductor& operator=( const CTraductor &c );

}; //CTraductor

#endif //__CTRADUCTOR_H__

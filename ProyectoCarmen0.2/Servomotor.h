/* 
* Servomotor.h
*
* Created: 10/10/2015 05:31:57 p. m.
* Author: omar
*/


#ifndef __SERVOMOTOR_H__
#define __SERVOMOTOR_H__

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "Servo.h"

class Servomotor
{
//variables
public:
#define ValorGiroPositivo 180//valor para que servo gire en el sentido contrario a las manecillas del reloj
#define ValorGiroNegativo 0//valor para que servo gire en el sentido de las manecillas del reloj

Servo MyServo;
int nPin;
protected:
private:

//functions
public:
Servomotor();
~Servomotor();
void Inicializar(int pin);
void GiroPositivo(void);
void GiroNegativo(void);
void Detener(void);
	
protected:
private:
	Servomotor( const Servomotor &c );
	Servomotor& operator=( const Servomotor &c );

}; //Servomotor

#endif //__SERVOMOTOR_H__

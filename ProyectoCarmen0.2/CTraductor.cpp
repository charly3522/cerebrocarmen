/* 
* CTraductor.cpp
*
* Created: 27/10/2015 06:39:37 p. m.
* Author: Carlos
*/



#include "CTraductor.h"
void CTraductor::Inicializar(int NumberOfSensors)
{
	Timer1.initialize();
	Timer1.nListaTimers[TimerOne::eTimerSensorU01] = 0;
	Timer1.nListaTimers[TimerOne::eTimerSensorU02] = 0;
	Timer1.nListaTimers[TimerOne::eTimerSensorU03] = 0;
	Timer1.nListaTimers[TimerOne::eTimerSensorU04] = 0;
	Timer1.nListaTimers[TimerOne::eTimerSensorU05] = 0;
	Timer1.nListaTimers[TimerOne::eTimerSensorU06] = 0;
	Timer1.nListaTimers[TimerOne::eTimerSensorU07] = 0;
	Timer1.nListaTimers[TimerOne::eTimerSensorU08] = 0;
	Timer1.nListaTimers[TimerOne::eTimerSensorU09] = 0;
	Timer1.nListaTimers[TimerOne::eTimerSensorU10] = 0;
	
	m_CantidadDeSensores = NumberOfSensors;
	for(int i = 0; i < m_CantidadDeSensores; i++)
	{
		m_CSU[i].Inicializar((i*2) + 22, (i*2) + 23); //se inicializa cada uno de los sensores
		m_snContadorMuestras[i] =0;
		m_nConjuntoDatos[i] = 0;
	}
	m_nCBDE = 1;
	CB = "";
}
void CTraductor::ObtenerDatosDeSensores()
{	
	for(int indexLTim = 0; indexLTim < m_CantidadDeSensores; indexLTim++) //iterador de datos de sensores
	{
		if(Timer1.nListaTimers[indexLTim] == 0) //S�lo entrar� cuando el timer del sensor haya concluido con sus 50ms
		{
			m_CSU[indexLTim].Captar(); //Caputra de datos del sensor
			m_nConjuntoDatos[indexLTim] = m_CSU[indexLTim].ObtenerDistancia();
			Timer1.nListaTimers[indexLTim] = 50; // 50 milisegundos de espera para cada sensor				
		}
	}
	CrearCadenaDeEstados();
}
void CTraductor::MostrarDatosSenUSerial()
{
		Serial.println(m_nCBDE);
	
}
void CTraductor::CrearCadenaDeEstados()
{
	m_nCBDE = 0;
	if(m_CantidadDeSensores >= 1)
		if(m_nConjuntoDatos[SU_SDI] < LIMITE_SU_SDI && m_nConjuntoDatos[SU_SDI] != 0)
			m_nCBDE = m_nCBDE | 0x01; //Operador OR por 0000000001 bin
			
	if(m_CantidadDeSensores >= 2)
	
		if(m_nConjuntoDatos[SU_SDD] < LIMITE_SU_SDD && m_nConjuntoDatos[SU_SDD] != 0)
			m_nCBDE = m_nCBDE | 0x02; //Operador OR por 0000000010 bin
			
	if(m_CantidadDeSensores >= 3)
		if(m_nConjuntoDatos[SU_SLI] < LIMITE_SU_SLI && m_nConjuntoDatos[SU_SLI] != 0)
			m_nCBDE = m_nCBDE | 0x04; //Operador OR por 0000000100 bin
	
	if(m_CantidadDeSensores >= 4)
		if(m_nConjuntoDatos[SU_SLD] < LIMITE_SU_SLD && m_nConjuntoDatos[SU_SLD] != 0)
			m_nCBDE = m_nCBDE | 0x08; //Operador OR por 0000001000 bin	
	
	if(m_CantidadDeSensores >= 5)
		if(m_nConjuntoDatos[SU_STI] < LIMITE_SU_STI && m_nConjuntoDatos[SU_STI] != 0)
			m_nCBDE = m_nCBDE | 0x10; //Operador OR por 0000010000 bin
	
	if(m_CantidadDeSensores >= 6)
		if(m_nConjuntoDatos[SU_STD] < LIMITE_SU_STD && m_nConjuntoDatos[SU_STD] != 0)
			m_nCBDE = m_nCBDE | 0x20; //Operador OR por 0000100000 bin
	
	if(m_CantidadDeSensores >= 7)
		if(m_nConjuntoDatos[SU_IDI] < LIMITE_SU_IDI && m_nConjuntoDatos[SU_IDI] != 0)
			m_nCBDE = m_nCBDE | 0x40; //Operador OR por 0001000000 bin
	
	if(m_CantidadDeSensores >= 8)
		if(m_nConjuntoDatos[SU_IDD] < LIMITE_SU_IDD && m_nConjuntoDatos[SU_IDD] != 0)
			m_nCBDE = m_nCBDE | 0x80; //Operador OR por 0010000000 bin
	
	if(m_CantidadDeSensores >= 9)
		if(m_nConjuntoDatos[SU_ITI] < LIMITE_SU_ITI && m_nConjuntoDatos[SU_ITI] != 0)
			m_nCBDE = m_nCBDE | 0x100; //Operador OR por 0100000000 bin	
	
	if(m_CantidadDeSensores >= 10)
		if(m_nConjuntoDatos[SU_ITD] < LIMITE_SU_ITD && m_nConjuntoDatos[SU_ITD] != 0)
			m_nCBDE = m_nCBDE | 0x200; //Operador OR por 1000000000 bin
}
unsigned int CTraductor::ObtenerCadenaDeEstados()
{
	return m_nCBDE;
}
// default constructor
CTraductor::CTraductor()
{
} //CTraductor

// default destructor
CTraductor::~CTraductor()
{
} //~CTraductor
